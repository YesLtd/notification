package yesltdcom.talha_jubayer.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import yesltdcom.talha_jubayer.notification.Data.DataContract;

public class MyFirebaseMessagingService extends FirebaseMessagingService{
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //insertData(remoteMessage.getNotification().getBody());
        sendMyNotification(remoteMessage.getNotification().getBody());
    }

    private void insertData(String message) {

        final String msg = message;

        //Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();

                ContentValues values = new ContentValues();

                values.put(DataContract.DataEntry.COLUMN_DATA_MESSAGE,msg);

                Uri newUri = getContentResolver().insert(DataContract.DataEntry.CONTENT_URI, values);
            }
        });

        ContentValues values = new ContentValues();

        values.put(DataContract.DataEntry.COLUMN_DATA_MESSAGE,message);

        Uri newUri = getContentResolver().insert(DataContract.DataEntry.CONTENT_URI, values);

    }

    private void sendMyNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Notification")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
