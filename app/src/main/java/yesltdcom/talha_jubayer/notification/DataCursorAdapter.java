package yesltdcom.talha_jubayer.notification;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import yesltdcom.talha_jubayer.notification.Data.DataContract;

public class DataCursorAdapter extends CursorAdapter{
    public DataCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        TextView messageTextView = view.findViewById(R.id.textView_msg);

        int msgColumnIndex = cursor.getColumnIndex(DataContract.DataEntry.COLUMN_DATA_MESSAGE);

        String msg = cursor.getString(msgColumnIndex);

        messageTextView.setText(msg);

    }
}
