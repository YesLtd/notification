package yesltdcom.talha_jubayer.notification.Data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class DataContract {

    private DataContract(){

    }

    public static final String CONTENT_AUTHORITY = "yesltdcom.talha_jubayer.notification";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_DATA = "data";

    public static final class DataEntry implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_DATA);

        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_DATA;

        public final static String TABLE_NAME = "data";

        public final static String _ID = BaseColumns._ID;

        public final static String COLUMN_DATA_MESSAGE ="name";

    }

}
