package yesltdcom.talha_jubayer.notification.Data;

public class Data {
    private String msg;

    public Data() {
    }

    public Data(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
