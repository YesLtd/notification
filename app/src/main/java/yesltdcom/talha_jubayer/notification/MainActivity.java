package yesltdcom.talha_jubayer.notification;

import android.content.ContentValues;
import android.database.Cursor;
import android.app.LoaderManager;
import android.content.Loader;
import android.content.CursorLoader;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import yesltdcom.talha_jubayer.notification.Data.Data;
import yesltdcom.talha_jubayer.notification.Data.DataContract;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    ListView listView;
    DataCursorAdapter cursorAdapter;

    private static final int PET_LOADER = 0;

    FirebaseDatabase database;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("notification");

        listView = findViewById(R.id.listView);

        cursorAdapter = new DataCursorAdapter(this,null);

        listView.setAdapter(cursorAdapter);

        getLoaderManager().initLoader(PET_LOADER, null, this);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int rowsDeleted = getContentResolver().delete(DataContract.DataEntry.CONTENT_URI, null, null);
                for(DataSnapshot myDataSnapshot : dataSnapshot.getChildren()){
                    String msg = myDataSnapshot.getValue(Data.class).getMsg();

                    ContentValues values = new ContentValues();

                    values.put(DataContract.DataEntry.COLUMN_DATA_MESSAGE,msg);

                    Uri newUri = getContentResolver().insert(DataContract.DataEntry.CONTENT_URI, values);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                DataContract.DataEntry._ID,
                DataContract.DataEntry.COLUMN_DATA_MESSAGE };

        return new CursorLoader(this, DataContract.DataEntry.CONTENT_URI, projection,null,null, null);
    }

    @Override
    public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor data) {
        cursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(android.content.Loader<Cursor> loader) {
        cursorAdapter.swapCursor(null);
    }

}
